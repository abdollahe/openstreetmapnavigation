//
// Created by abdollah on 18/12/19.
//

#include <utility>
#include "A_Star_Searcher.h"


void A_Star_Searcher::setBoardFilePath(std::__cxx11::string filePath) {
   A_Star_Searcher::inputBoardPath = std::move(filePath) ;
}

void A_Star_Searcher::readBoardFile() {
    ifstream boardFile(A_Star_Searcher::inputBoardPath) ;
    vector<BoardStates> tempBoardLine ;

    if(boardFile) {
        string line ;

        while( getline(boardFile , line)) {
            tempBoardLine = parseBoardLine(line) ;
            boardMap.push_back(tempBoardLine) ;
        }
    }
}

vector<BoardStates> A_Star_Searcher::parseBoardLine(string line) {

    vector<BoardStates> boardLine ;

    while(!line.empty()) {
        long index = line.find_first_of(',') ;

        // If the string contains only one character (final character in the line is left)
        if(index == -1) {
            if(stoi(line) == 0)
                boardLine.push_back(BoardStates::kEmpty) ;
            else
                boardLine.push_back(BoardStates::kObstacle) ;

            line.clear() ;
        }

        // Else if line has multiple characters
        else {
            string value = line.substr(0 , index) ;

            if(stoi(value) == 0)
                boardLine.push_back(BoardStates::kEmpty) ;
            else
                boardLine.push_back(BoardStates::kObstacle) ;

            line.erase(0 , index + 1) ;
        }
    }
    return boardLine ;
}

vector<BoardStates> A_Star_Searcher::parseBoardLineV2(const string& line) {
    vector<BoardStates> boardLine ;

    istringstream iss(line) ;
    int n ;
    char c ;

    while(iss >> n >> c && c ==',') {
        if(n == 0)
            boardLine.push_back(BoardStates::kEmpty) ;
        else
            boardLine.push_back(BoardStates::kObstacle) ;
    }

    return boardLine ;
}
