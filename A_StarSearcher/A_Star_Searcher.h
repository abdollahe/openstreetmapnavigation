//
// Created by abdollah on 18/12/19.
//

#ifndef OPENSTREETMAPPROJECT_A_STAR_SEARCHER_H
#define OPENSTREETMAPPROJECT_A_STAR_SEARCHER_H

#include "iostream"
#include "string"
#include "vector"
#include "fstream"
#include "sstream"


using namespace std;


enum class BoardStates {
    kEmpty,
    kObstacle
};

class A_Star_Searcher {
//----------
// Variables
private:
    // Variable to hold the path of the source board map
    string inputBoardPath;

    //variable to hold the board map representation as a 2D matrix
    vector<vector<BoardStates>> boardMap;

//-----------
// Methods
public:
    A_Star_Searcher() : inputBoardPath("") {
        cout << "A Star Search object created successfully" << endl;
    }

    /**
     * This method is used to set the path for the source board file
     * @param filePath - path to the input file
     */
    void setBoardFilePath(string filePath);

    /**
     * This method is used to read the contents of the input file and create an in memory
     * representation of it using a s dimensional matrix that each state is represented using the enum values
     * defined in the header file
     */
    void readBoardFile();

private:

    /**
     * This method is written using basic C++ to parse every line of the source file and create a
     * 1 dimensional vector representation of the line
     * @param line - value holding the line to be parsed
     * @return  - vector representation of the line parsed
     */
    static vector<BoardStates> parseBoardLine(string line);

    /**
     * This method parses the line read from the input file in to a 1 dimensional vector using the sstream libraries
     * @param line - value holding the line to be parsed
     * @return  - vector representation of the line parsed
     */
    static vector<BoardStates> parseBoardLineV2(const string& line) ;
};

#endif //OPENSTREETMAPPROJECT_A_STAR_SEARCHER_H
